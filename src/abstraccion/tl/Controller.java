package abstraccion.tl;

import abstraccion.bl.Clientes;
import abstraccion.bl.Cuenta;
import abstraccion.bl.CuentaAhorro;
import abstraccion.bl.CuentaCorriente;
import abstraccion.bl.Movimiento;
import abstraccion.dl.CapaLogica;
import java.time.LocalDate;

public class Controller {

    CapaLogica logica = new CapaLogica();

    public void procesarClientes(String nombre, String identificacion, String direccion) {
        Clientes tmpCliente = new Clientes();

        tmpCliente.setNombre(nombre);
        tmpCliente.setIdentificacion(identificacion);
        tmpCliente.setDireccion(direccion);

        logica.registrarCliente(tmpCliente);

    }

    public String[] listarCliente() {
        return logica.getCliente();
    }

    public void procesarCuenta(String numCuenta, LocalDate fechaApertura, double depositoInicial,Clientes cliente) {
        Cuenta tmpCuenta;
        tmpCuenta = new Cuenta(numCuenta, fechaApertura, depositoInicial,cliente);

        logica.registrarCuenta(tmpCuenta);
    }

    public void procesarCuentaCorriente(String numCuenta, LocalDate fechaApertura, double depositoInicial) {
        CuentaCorriente tmpCuentaCorriente;
        tmpCuentaCorriente = new CuentaCorriente(numCuenta, fechaApertura, depositoInicial);

        logica.registrarCuentaCorriente(tmpCuentaCorriente);

    }

    public void procesarCuentaAhorro(String numCuenta, LocalDate fechaApertura, double depositoInicial,Clientes cliente) {
        CuentaAhorro tmpCuentaAhorro;
        tmpCuentaAhorro = new CuentaAhorro(numCuenta, fechaApertura, depositoInicial);

        logica.registrarCuentaAhorro(tmpCuentaAhorro);

    }

    public String[] listarCuenta() {
        return logica.getCuenta();
    }
    
    public void procesarMovimineto(LocalDate fecha, String descripcion, double monto, Cuenta cuentas){
        Movimiento tmpMovimiento;
        tmpMovimiento=new Movimiento(fecha,descripcion,monto,cuentas);
        
        logica.registrarMovimiento(tmpMovimiento);
        
    }
    
    public String[] listarMovimiento(){
        return logica.getMovimineto();
    }

    public boolean validacionCliente(String identificacion) {
        boolean bandera = true;
        bandera = logica.validacionCliente(identificacion);
        return bandera;
    }//Fin validacionCliente

}
