package abstraccion.dl;

import abstraccion.bl.Cuenta;
import abstraccion.bl.CuentaAhorro;
import abstraccion.bl.CuentaCorriente;
import abstraccion.bl.Clientes;
import abstraccion.bl.Movimiento;
import static java.lang.System.out;
import java.util.ArrayList;

public class CapaLogica {

    ArrayList<Clientes> cliente = new ArrayList<>();
    ArrayList<Cuenta> cuenta = new ArrayList<>();
    ArrayList<CuentaAhorro> cuentaAhorro = new ArrayList<>();
    ArrayList<CuentaCorriente> cuentaCorriente = new ArrayList<>();
    ArrayList<Movimiento> movimiento = new ArrayList<>();

    public void registrarCliente(Clientes obj) {

        //agregar el elemento al array list
        cliente.add(obj);

    }

    public void registrarCuentaCorriente(CuentaCorriente obj) {

        //agregar el elemento al array list
        cuentaCorriente.add(obj);

    }

    public void registrarCuentaAhorro(CuentaAhorro obj) {

        //agregar el elemento al array list
        cuentaAhorro.add(obj);

    }

    public void registrarMovimiento(Movimiento obj) {
        movimiento.add(obj);

    }

    public String[] getCliente() {
        String[] data = new String[cliente.size()];
        int posicion = 0;
        //recorrer araylist con for each

        for (Clientes dato : cliente) {
            String info = dato.getNombre() + " " + dato.getIdentificacion() + " " + dato.getDireccion();

            data[posicion] = info;
            posicion++;

        }
        return data;
    }

    public boolean validacionCliente(String identificacion) {
        boolean bandera = false;
        for (Clientes dato : cliente) {
            String identificacion2 = dato.getIdentificacion();
            if (identificacion.equals(identificacion2)) {
                bandera = true;
            }
        }
        return bandera;
    }

    public void registrarCuenta(Cuenta obj) {
        //agregar el elemento al array list
        double deposito = obj.getDepositoInicial();
        if (deposito >= 50000) {
            cuenta.add(obj);
        } else {
            out.println("El monto es menor al necesario");
        }

    }

    public String[] getCuenta() {
        String[] data = new String[cuenta.size()];
        int posicion = 0;
        //recorrer araylist con for each

        for (Cuenta dato : cuenta) {
            String info = dato.toString();
            data[posicion] = info;
            posicion++;
        }
        return data;
    }

    public String[] getMovimineto() {
        String[] data = new String[movimiento.size()];
        int posicion = 0;

        for (Movimiento dato : movimiento) {
            String info = dato.toString();
            data[posicion] = info;
            posicion++;
        }
        return data;
    }

}
