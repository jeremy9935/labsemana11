package abstraccion.cl;


import abstraccion.bl.Clientes;
import java.util.ArrayList;

public class CapaLogica {

    ArrayList<Clientes> cliente = new ArrayList<>();

    public void registrarCliente(Clientes obj) {
        //agregar el elemento al array list

        cliente.add(obj);
    }

    public String[] getCliente() {
        String[] data = new String[cliente.size()];
        int posicion = 0;
        //recorrer araylist con for each

        for (Clientes dato : cliente) {
            String info = dato.getNombre() + " " + dato.getIdentificacion() + " " + dato.getDireccion();

            data[posicion] = info;
            posicion++;

        }
        return data;
    }

}
