package abstraccion.bl;

import java.time.LocalDate;

public class CuentaCorriente extends Cuenta {

    public CuentaCorriente() {
        super();
    }

    public CuentaCorriente(String numCuenta, LocalDate fechaApertura, double depositoInicial) {
        super(numCuenta, fechaApertura, depositoInicial);
    }

}
