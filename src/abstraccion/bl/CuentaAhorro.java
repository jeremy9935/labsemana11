package abstraccion.bl;

import java.time.LocalDate;

public class CuentaAhorro extends Cuenta {

    public CuentaAhorro() {
    }

    public CuentaAhorro(String numCuenta, LocalDate fechaApertura, double depositoInicial) {
        super(numCuenta, fechaApertura, depositoInicial);
    }

}
