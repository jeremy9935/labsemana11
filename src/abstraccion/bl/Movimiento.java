package abstraccion.bl;

import java.time.LocalDate;

public  class Movimiento {

    private LocalDate fecha;
    private String descripcion;
    private double monto;
    Cuenta cuentas;

    public Movimiento() {

    }

    public Movimiento(LocalDate fecha, String descripcion, double monto) {
        this.fecha = fecha;
        this.descripcion = descripcion;
        this.monto = monto;
    }

    public Movimiento(LocalDate fecha, String descripcion, double monto, Cuenta cuentas) {
        this.fecha = fecha;
        this.descripcion = descripcion;
        this.monto = monto;
        this.cuentas = cuentas;
    }

    public Cuenta getCuentas() {
        return cuentas;
    }

    public void setCuentas(Cuenta cuentas) {
        this.cuentas = cuentas;
    }
    

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    @Override
    public String toString() {
        return "Movimiento{" + "fecha=" + fecha + ", descripcion=" + descripcion + ", monto=" + monto + ", cuentas=" + cuentas + '}';
    }

    }


