
package abstraccion.bl;

import abstraccion.cl.CapaLogica;
import abstraccion.bl.Clientes;

public class Controller {
    
    
    CapaLogica logica = new CapaLogica();
    
    public void procesarClientes(String nombre, String identificacion, String direccion){
        Clientes tmpCliente = new Clientes();
        
        tmpCliente.setNombre(nombre);
        tmpCliente.setIdentificacion(identificacion);
        tmpCliente.setDireccion(direccion);
        
        logica.registrarCliente(tmpCliente);
        
    }
    
    public String[] listarCliente() {
        return logica.getCliente();
    }
    
    
    
}
