package abstraccion.bl;

import java.time.LocalDate;

public class Cuenta {

    protected String numCuenta;
    protected LocalDate fechaApertura;
    protected double depositoInicial;
    Clientes cliente;
    //protected double retiro;

    public Cuenta(String numCuenta,  LocalDate fechaApertura, double depositoInicial, Clientes cliente) {
        this.numCuenta = numCuenta;
        this.fechaApertura = fechaApertura;
        this.depositoInicial = depositoInicial;
        this.cliente = cliente;
    }

    private static final String ARCHIVOCUENTA = "archivoCuenta.txt";

    public Cuenta() {
    }

    public Cuenta(double saldo, LocalDate fechaApertura, double depositoInicial) {
      
        this.fechaApertura = fechaApertura;
        this.depositoInicial = depositoInicial;
    }

    
    public Cuenta(String numCuenta, LocalDate fechaApertura, double depositoInicial) {
        this.numCuenta = numCuenta;
        this.fechaApertura = fechaApertura;
        this.depositoInicial = depositoInicial;
        //this.retiro = retiro;
    }

    public String getNumCuenta() {
        return numCuenta;
    }

    public void setNumCuenta(String numCuenta) {
        this.numCuenta = numCuenta;
    }

    public LocalDate getFechaApertura() {
        return fechaApertura;
    }

    public void setFechaApertura(LocalDate fechaApertura) {
        this.fechaApertura = fechaApertura;
    }

    public double getDepositoInicial() {
        return depositoInicial;
    }

    public void setDepositoInicial(double depositoInicial) {
        this.depositoInicial = depositoInicial;
    }

    public Clientes getCliente() {
        return cliente;
    }

    public void setCliente(Clientes cliente) {
        this.cliente = cliente;
    }
    

    public void calcularDepositoInicial() {

    }

    @Override
    public String toString() {
        return "Cuenta{" + "numCuenta=" + numCuenta + ", fechaApertura=" + fechaApertura + ", depositoInicial=" + depositoInicial + ", cliente=" + cliente + '}';
    }

    

    

}
