package abstraccion.UI;

import abstraccion.bl.Clientes;
import abstraccion.bl.Cuenta;
import abstraccion.bl.Movimiento;
import abstraccion.tl.Controller;

import java.io.*;
import java.time.LocalDate;

public class Main {

    public static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    public static PrintStream out = System.out;

    static Controller gestor = new Controller();
// se crean los archivos de texto para guardar los datos
    static final String ARCHIVO_CLIENTE = "Archivo-Cliente.txt";
    static final String ARCHIVO_CUENTA = "Archivo-Cuenta.txt";
    static final String ARCHIVO_MOVIMIENTO = "Archivo-Movimiento.txt";

    public static void main(String[] args) throws IOException {
        crearMenu();
    }

    public static void crearMenu() throws IOException {
        boolean noSalir;
        int opcion;
        do {
            mostrarMenu();
            opcion = leerOpcion();
            noSalir = ejecutarAccion(opcion);

        } while (noSalir);

    }

    public static void mostrarMenu() throws IOException {

        out.println("1.    Registrar Cliente");
        out.println("2.    Listar Clientes");
        out.println("3.    Registrar Cuenta");
        out.println("4.    Listar Cuenta");
        out.println("5.    Registrar Movimiento");
        out.println("6.    ");
        out.println("7.    Listar Movimineto");
        out.println("8.    Listar moviminetos");
        out.println("9.    Salir");

        out.println("");
    }// FIN MOSTRAR  MENÚ

    public static int leerOpcion() throws IOException {
        int opcion;

        out.println("Seleccione su opción que desee:\n");
        opcion = Integer.parseInt(in.readLine());
        out.println();
        return opcion;

    }// FIN LEER OPCION

    public static boolean ejecutarAccion(int popcion) throws IOException {
        boolean noSalir = true;
        boolean define;
        switch (popcion) {

            case 1:
                registrarCliente();
                break;
            case 2:
                listarClinetes();
                break;
            case 3:
                registrarCuenta();
                break;
            case 4:
                listarCuenta();
                break;
            case 5:
                registrarMovimientoRetiro();
                break;
            case 6:
                listarMovimiento();
                break;
            case 7:
                registrarMovimientoRetiro();
                break;
            case 8:
                listarMovimiento();
                break;
            case 9:
                out.println("Adiós");
                noSalir = false;
                break;

            default:
                out.println("¡OPCIÓN INVÁLIDA!" + "\n"
                        + "Por favor inténtelo nuevamente");
                out.println();
                break;
        }

        return noSalir;

    }// Fin de 

    public static void crearArchivo() throws IOException {
        File f = new File(ARCHIVO_CLIENTE);

        if (f.createNewFile()) {
            out.println("Archivo creado exitosamente");
        } else {
            out.println("El archivo ya existe");

        }
    }

    public static void crearArchivo2() throws IOException {
        File f = new File(ARCHIVO_MOVIMIENTO);

        if (f.createNewFile()) {
            out.println("Archivo creado exitosamente");
        } else {
            out.println("El archivo ya existe");

        }
    }

    public static void registrarCliente() throws IOException {
//        try {
//
//            FileWriter writer = new FileWriter(ARCHIVO_CLIENTE, true);
//            BufferedWriter buffer = new BufferedWriter(writer);
        String nombre, identificacion, direccion;
        out.println("Digite el nombre del dueño de la cuenta");
        nombre = in.readLine();
        out.println("Digite la identificación");
        identificacion = in.readLine();
        out.println("Digite la dirreción");
        direccion = in.readLine();

        gestor.procesarClientes(nombre, identificacion, direccion);
//            Clientes info = new Clientes(nombre, identificacion, direccion);
//            buffer.write(info.toString());
//            buffer.newLine();
//            buffer.close();
//        } catch (IOException e) {
//            e.fillInStackTrace();
//        }

    }

    public static void listarClinetes() throws IOException {

//        try {
//            FileReader reader = new FileReader(ARCHIVO_CLIENTE);
//            BufferedReader buffer = new BufferedReader(reader);
//            String datos;
//
//            while ((datos = buffer.readLine()) != null) {
//                out.println(datos);
//            }
//            reader.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        String[] cliente = gestor.listarCliente();

        Clientes p = new Clientes();
        out.println(p);

        for (String dato : cliente) {
            out.println(dato);

        }
    }

    public static void registrarCuenta() throws IOException {

        String numCuenta;
        double saldo;
        LocalDate fechaApertura;
        double depositoInicial;
        boolean bandera = false;
        Clientes clie = new Clientes("mariana", "12", "ajauela");
        Cuenta c = new Cuenta();
        out.println("Digite la identifiacacion del cliente al que desea crearle la cuenta");
        String id = in.readLine();

        bandera = gestor.validacionCliente(id);
        if (bandera == true) {
            out.println("Digite la cuenta que desa registrar");
            out.println("1. Cuenta Corriente, 2.Cuenta de ahorro, 3.Cuenta de ahorro programado");
            int numero = Integer.parseInt(in.readLine());
//      
//            try {
//
//                FileWriter writer = new FileWriter(ARCHIVO_CUENTA, true);
//                BufferedWriter buffer = new BufferedWriter(writer);

            if (numero == 1) {
                out.println("Digite el numero de cuenta");
                numCuenta = in.readLine();
                out.println("Digite la fecha de apertura");
                out.println("Con el formato YYYY-MM-DD");
                String fechaNInput = in.readLine();
                fechaApertura = LocalDate.parse(fechaNInput);
                out.println("Digite el deposito inicial");
                depositoInicial = Integer.parseInt(in.readLine());
                gestor.procesarCuenta(numCuenta, fechaApertura, depositoInicial, clie);
                c = new Cuenta(numCuenta, fechaApertura, depositoInicial, clie);
            } else if (numero == 2) {
                out.println("Digite el numero de cuenta");
                numCuenta = in.readLine();
                out.println("Digite la fecha de apertura");
                out.println("Con el formato YYYY-MM-DD");
                String fechaNInput = in.readLine();
                fechaApertura = LocalDate.parse(fechaNInput);
                out.println("Digite el deposito inicial");
                depositoInicial = Integer.parseInt(in.readLine());
                gestor.procesarCuentaAhorro(numCuenta, fechaApertura, depositoInicial, clie);
                c = new Cuenta(numCuenta, fechaApertura, depositoInicial);
            } else if (numero == 3) {
                out.println("Digite el numero de cuenta");
                numCuenta = in.readLine();
                out.println("Digite la fecha de apertura");
                out.println("Con el formato YYYY-MM-DD");
                String fechaNInput = in.readLine();
                fechaApertura = LocalDate.parse(fechaNInput);
                out.println("Digite el deposito inicial");
                depositoInicial = Integer.parseInt(in.readLine());
                gestor.procesarCuenta(numCuenta, fechaApertura, depositoInicial, clie);
                c = new Cuenta(numCuenta, fechaApertura, depositoInicial);
            } else if (numero != 1 || numero != 2 || numero != 3) {
                out.println("OPCIÓN INVALIDAD");

            }

//                buffer.write(c.toString());
//                buffer.newLine();
//                buffer.close();
//            } catch (IOException e) {
//                e.fillInStackTrace();
//            }
        } else {
            out.println("Este cliente no esta registrado");
        }
    }

    public static void listarCuenta() throws IOException {
//        try {
//            FileReader reader = new FileReader(ARCHIVO_CUENTA);
//            BufferedReader buffer = new BufferedReader(reader);
//            String datos;
//
//            while ((datos = buffer.readLine()) != null) {
//                out.println(datos);
//            }
//            reader.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        String[] cuenta = gestor.listarCuenta();

        Cuenta p = new Cuenta();
        out.println(p);

        for (String dato : cuenta) {
            out.println(dato);

        }
    }

    public static void registrarMovimiento() {

    }

    public static void registrarMovimientoRetiro() throws IOException {
//        try {
//
//            FileWriter writer = new FileWriter(ARCHIVO_MOVIMIENTO, true);
//            BufferedWriter buffer = new BufferedWriter(writer);

        boolean bandera = false;
        out.println("Digite la identifiacacion del cliente al que desea crearle la cuenta");
        String id = in.readLine();

        bandera = gestor.validacionCliente(id);
        if (bandera == true) {
            String descripcion;
            double monto;
            LocalDate fecha;
            Cuenta cuentas = new Cuenta();

            out.println("La descripcion de la accion");
            descripcion = in.readLine();
            out.println("Digite la fecha ");
            out.println("Con el formato YYYY-MM-DD");
            String fechaNInput = in.readLine();
            fecha = LocalDate.parse(fechaNInput);
            out.println("Digite el monto a retirar");
            monto = Integer.parseInt(in.readLine());

            gestor.procesarMovimineto(fecha, descripcion, monto, cuentas);
            Movimiento m= new Movimiento(fecha, descripcion, monto,cuentas) ;
            
            out.println(m);
        }
//            buffer.write(info.toString());
//            buffer.newLine();
//            buffer.close();
//        } catch (IOException e) {
//            e.fillInStackTrace();
//        }
    }

    public static void listarMovimiento() {
//        try {
//            FileReader reader = new FileReader(ARCHIVO_MOVIMIENTO);
//            BufferedReader buffer = new BufferedReader(reader);
//            String datos;
//
//            while ((datos = buffer.readLine()) != null) {
//                out.println(datos);
//            }
//            reader.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

        String[] movimiento = gestor.listarMovimiento();

        Movimiento p = new Movimiento();
        out.println(p);

        for (String dato : movimiento) {
            out.println(dato);

        }
    }
}
